(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"/home/bigdrop/Documents/project/gulp4/assets/js/global.js":[function(require,module,exports){
(function (global){
"use strict";

var _global = require("./modules/global");

var _global2 = _interopRequireDefault(_global);

var _HOME = require("./modules/HOME");

var _HOME2 = _interopRequireDefault(_HOME);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

document.body.className += 'ontouchstart' in window ? 'touch' : 'no-touch';

var init = function init() {},
    load = function load() {},
    scroll = function scroll() {};

if (global.vars !== undefined) {
    switch (global.vars.page) {
        case 'home_page':
            init = _HOME2.default.init.bind(_HOME2.default);
            break;
    }
}

$(document).ready(_global2.default.init(), init());

// $(window).scroll(GLOBAL.scroll);
//
// $(window).resize(GLOBAL.resize);
//
// $(window).on('load', ()=>{
//     GLOBAL.load();
//     load();
// });

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./modules/HOME":"/home/bigdrop/Documents/project/gulp4/assets/js/modules/HOME.js","./modules/global":"/home/bigdrop/Documents/project/gulp4/assets/js/modules/global.js"}],"/home/bigdrop/Documents/project/gulp4/assets/js/modules/HOME.js":[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    init: function init() {
        console.log('home init');
    }
};

},{}],"/home/bigdrop/Documents/project/gulp4/assets/js/modules/global.js":[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    init: function init() {
        console.log('global init');
    }
};

},{}]},{},["/home/bigdrop/Documents/project/gulp4/assets/js/global.js"])

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhc3NldHMvanMvZ2xvYmFsLmpzIiwiYXNzZXRzL2pzL21vZHVsZXMvSE9NRS5qcyIsImFzc2V0cy9qcy9tb2R1bGVzL2dsb2JhbC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7OztBQ0FBOzs7O0FBQ0E7Ozs7OztBQUVBLFNBQVMsSUFBVCxDQUFjLFNBQWQsSUFBNEIsa0JBQWtCLE1BQW5CLEdBQTZCLE9BQTdCLEdBQXVDLFVBQWxFOztBQUVBLElBQUksT0FBTyxnQkFBVSxDQUFFLENBQXZCO0FBQUEsSUFDSSxPQUFPLFNBQVAsSUFBTyxHQUFVLENBQUUsQ0FEdkI7QUFBQSxJQUVJLFNBQVMsU0FBVCxNQUFTLEdBQVUsQ0FBRSxDQUZ6Qjs7QUFJQSxJQUFHLE9BQU8sSUFBUCxLQUFnQixTQUFuQixFQUE4QjtBQUMxQixZQUFRLE9BQU8sSUFBUCxDQUFZLElBQXBCO0FBQ0ksYUFBSyxXQUFMO0FBQ0ksbUJBQU8sZUFBSyxJQUFMLENBQVUsSUFBVixnQkFBUDtBQUNBO0FBSFI7QUFLSDs7QUFFRCxFQUFFLFFBQUYsRUFBWSxLQUFaLENBQWtCLGlCQUFPLElBQVAsRUFBbEIsRUFBaUMsTUFBakM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztrQkMxQmU7QUFDWCxRQURXLGtCQUNMO0FBQ0YsZ0JBQVEsR0FBUixDQUFZLFdBQVo7QUFDSDtBQUhVLEM7Ozs7Ozs7O2tCQ0FBO0FBQ1gsUUFEVyxrQkFDTDtBQUNGLGdCQUFRLEdBQVIsQ0FBWSxhQUFaO0FBQ0g7QUFIVSxDIiwiZmlsZSI6Imdsb2JhbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiaW1wb3J0IEdMT0JBTCBmcm9tIFwiLi9tb2R1bGVzL2dsb2JhbFwiO1xuaW1wb3J0IEhPTUUgZnJvbSBcIi4vbW9kdWxlcy9IT01FXCI7XG5cbmRvY3VtZW50LmJvZHkuY2xhc3NOYW1lICs9ICgnb250b3VjaHN0YXJ0JyBpbiB3aW5kb3cpID8gJ3RvdWNoJyA6ICduby10b3VjaCc7XG5cbmxldCBpbml0ID0gZnVuY3Rpb24oKXt9LFxuICAgIGxvYWQgPSBmdW5jdGlvbigpe30sXG4gICAgc2Nyb2xsID0gZnVuY3Rpb24oKXt9O1xuXG5pZihnbG9iYWwudmFycyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgc3dpdGNoIChnbG9iYWwudmFycy5wYWdlKSB7XG4gICAgICAgIGNhc2UgJ2hvbWVfcGFnZSc6XG4gICAgICAgICAgICBpbml0ID0gSE9NRS5pbml0LmJpbmQoSE9NRSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICB9XG59XG5cbiQoZG9jdW1lbnQpLnJlYWR5KEdMT0JBTC5pbml0KCksIGluaXQoKSk7XG5cbi8vICQod2luZG93KS5zY3JvbGwoR0xPQkFMLnNjcm9sbCk7XG4vL1xuLy8gJCh3aW5kb3cpLnJlc2l6ZShHTE9CQUwucmVzaXplKTtcbi8vXG4vLyAkKHdpbmRvdykub24oJ2xvYWQnLCAoKT0+e1xuLy8gICAgIEdMT0JBTC5sb2FkKCk7XG4vLyAgICAgbG9hZCgpO1xuLy8gfSk7IiwiZXhwb3J0IGRlZmF1bHQge1xuICAgIGluaXQoKXtcbiAgICAgICAgY29uc29sZS5sb2coJ2hvbWUgaW5pdCcpO1xuICAgIH0sXG59O1xuIiwiZXhwb3J0IGRlZmF1bHQge1xuICAgIGluaXQoKXtcbiAgICAgICAgY29uc29sZS5sb2coJ2dsb2JhbCBpbml0Jyk7XG4gICAgfVxufTsiXSwicHJlRXhpc3RpbmdDb21tZW50IjoiLy8jIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2NoYXJzZXQ9dXRmLTg7YmFzZTY0LGV5SjJaWEp6YVc5dUlqb3pMQ0p6YjNWeVkyVnpJanBiSW01dlpHVmZiVzlrZFd4bGN5OWljbTkzYzJWeUxYQmhZMnN2WDNCeVpXeDFaR1V1YW5NaUxDSmhjM05sZEhNdmFuTXZaMnh2WW1Gc0xtcHpJaXdpWVhOelpYUnpMMnB6TDIxdlpIVnNaWE12U0U5TlJTNXFjeUlzSW1GemMyVjBjeTlxY3k5dGIyUjFiR1Z6TDJkc2IySmhiQzVxY3lKZExDSnVZVzFsY3lJNlcxMHNJbTFoY0hCcGJtZHpJam9pUVVGQlFUczdPenRCUTBGQk96czdPMEZCUTBFN096czdPenRCUVVWQkxGTkJRVk1zU1VGQlZDeERRVUZqTEZOQlFXUXNTVUZCTkVJc2EwSkJRV3RDTEUxQlFXNUNMRWRCUVRaQ0xFOUJRVGRDTEVkQlFYVkRMRlZCUVd4Rk96dEJRVVZCTEVsQlFVa3NUMEZCVHl4blFrRkJWU3hEUVVGRkxFTkJRWFpDTzBGQlFVRXNTVUZEU1N4UFFVRlBMRk5CUVZBc1NVRkJUeXhIUVVGVkxFTkJRVVVzUTBGRWRrSTdRVUZCUVN4SlFVVkpMRk5CUVZNc1UwRkJWQ3hOUVVGVExFZEJRVlVzUTBGQlJTeERRVVo2UWpzN1FVRkpRU3hKUVVGSExFOUJRVThzU1VGQlVDeExRVUZuUWl4VFFVRnVRaXhGUVVFNFFqdEJRVU14UWl4WlFVRlJMRTlCUVU4c1NVRkJVQ3hEUVVGWkxFbEJRWEJDTzBGQlEwa3NZVUZCU3l4WFFVRk1PMEZCUTBrc2JVSkJRVThzWlVGQlN5eEpRVUZNTEVOQlFWVXNTVUZCVml4blFrRkJVRHRCUVVOQk8wRkJTRkk3UVVGTFNEczdRVUZGUkN4RlFVRkZMRkZCUVVZc1JVRkJXU3hMUVVGYUxFTkJRV3RDTEdsQ1FVRlBMRWxCUVZBc1JVRkJiRUlzUlVGQmFVTXNUVUZCYWtNN08wRkJSVUU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHM3T3pzN096czdPenRyUWtNeFFtVTdRVUZEV0N4UlFVUlhMR3RDUVVOTU8wRkJRMFlzWjBKQlFWRXNSMEZCVWl4RFFVRlpMRmRCUVZvN1FVRkRTRHRCUVVoVkxFTTdPenM3T3pzN08ydENRMEZCTzBGQlExZ3NVVUZFVnl4clFrRkRURHRCUVVOR0xHZENRVUZSTEVkQlFWSXNRMEZCV1N4aFFVRmFPMEZCUTBnN1FVRklWU3hESWl3aVptbHNaU0k2SW1kbGJtVnlZWFJsWkM1cWN5SXNJbk52ZFhKalpWSnZiM1FpT2lJaUxDSnpiM1Z5WTJWelEyOXVkR1Z1ZENJNld5SW9ablZ1WTNScGIyNGdaU2gwTEc0c2NpbDdablZ1WTNScGIyNGdjeWh2TEhVcGUybG1LQ0Z1VzI5ZEtYdHBaaWdoZEZ0dlhTbDdkbUZ5SUdFOWRIbHdaVzltSUhKbGNYVnBjbVU5UFZ3aVpuVnVZM1JwYjI1Y0lpWW1jbVZ4ZFdseVpUdHBaaWdoZFNZbVlTbHlaWFIxY200Z1lTaHZMQ0V3S1R0cFppaHBLWEpsZEhWeWJpQnBLRzhzSVRBcE8zWmhjaUJtUFc1bGR5QkZjbkp2Y2loY0lrTmhibTV2ZENCbWFXNWtJRzF2WkhWc1pTQW5YQ0lyYnl0Y0lpZGNJaWs3ZEdoeWIzY2daaTVqYjJSbFBWd2lUVTlFVlV4RlgwNVBWRjlHVDFWT1JGd2lMR1o5ZG1GeUlHdzlibHR2WFQxN1pYaHdiM0owY3pwN2ZYMDdkRnR2WFZzd1hTNWpZV3hzS0d3dVpYaHdiM0owY3l4bWRXNWpkR2x2YmlobEtYdDJZWElnYmoxMFcyOWRXekZkVzJWZE8zSmxkSFZ5YmlCektHNC9ianBsS1gwc2JDeHNMbVY0Y0c5eWRITXNaU3gwTEc0c2NpbDljbVYwZFhKdUlHNWJiMTB1Wlhod2IzSjBjMzEyWVhJZ2FUMTBlWEJsYjJZZ2NtVnhkV2x5WlQwOVhDSm1kVzVqZEdsdmJsd2lKaVp5WlhGMWFYSmxPMlp2Y2loMllYSWdiejB3TzI4OGNpNXNaVzVuZEdnN2J5c3JLWE1vY2x0dlhTazdjbVYwZFhKdUlITjlLU0lzSW1sdGNHOXlkQ0JIVEU5Q1FVd2dabkp2YlNCY0lpNHZiVzlrZFd4bGN5OW5iRzlpWVd4Y0lqdGNibWx0Y0c5eWRDQklUMDFGSUdaeWIyMGdYQ0l1TDIxdlpIVnNaWE12U0U5TlJWd2lPMXh1WEc1a2IyTjFiV1Z1ZEM1aWIyUjVMbU5zWVhOelRtRnRaU0FyUFNBb0oyOXVkRzkxWTJoemRHRnlkQ2NnYVc0Z2QybHVaRzkzS1NBL0lDZDBiM1ZqYUNjZ09pQW5ibTh0ZEc5MVkyZ25PMXh1WEc1c1pYUWdhVzVwZENBOUlHWjFibU4wYVc5dUtDbDdmU3hjYmlBZ0lDQnNiMkZrSUQwZ1puVnVZM1JwYjI0b0tYdDlMRnh1SUNBZ0lITmpjbTlzYkNBOUlHWjFibU4wYVc5dUtDbDdmVHRjYmx4dWFXWW9aMnh2WW1Gc0xuWmhjbk1nSVQwOUlIVnVaR1ZtYVc1bFpDa2dlMXh1SUNBZ0lITjNhWFJqYUNBb1oyeHZZbUZzTG5aaGNuTXVjR0ZuWlNrZ2UxeHVJQ0FnSUNBZ0lDQmpZWE5sSUNkb2IyMWxYM0JoWjJVbk9seHVJQ0FnSUNBZ0lDQWdJQ0FnYVc1cGRDQTlJRWhQVFVVdWFXNXBkQzVpYVc1a0tFaFBUVVVwTzF4dUlDQWdJQ0FnSUNBZ0lDQWdZbkpsWVdzN1hHNGdJQ0FnZlZ4dWZWeHVYRzRrS0dSdlkzVnRaVzUwS1M1eVpXRmtlU2hIVEU5Q1FVd3VhVzVwZENncExDQnBibWwwS0NrcE8xeHVYRzR2THlBa0tIZHBibVJ2ZHlrdWMyTnliMnhzS0VkTVQwSkJUQzV6WTNKdmJHd3BPMXh1THk5Y2JpOHZJQ1FvZDJsdVpHOTNLUzV5WlhOcGVtVW9SMHhQUWtGTUxuSmxjMmw2WlNrN1hHNHZMMXh1THk4Z0pDaDNhVzVrYjNjcExtOXVLQ2RzYjJGa0p5d2dLQ2s5UG50Y2JpOHZJQ0FnSUNCSFRFOUNRVXd1Ykc5aFpDZ3BPMXh1THk4Z0lDQWdJR3h2WVdRb0tUdGNiaTh2SUgwcE95SXNJbVY0Y0c5eWRDQmtaV1poZFd4MElIdGNiaUFnSUNCcGJtbDBLQ2w3WEc0Z0lDQWdJQ0FnSUdOdmJuTnZiR1V1Ykc5bktDZG9iMjFsSUdsdWFYUW5LVHRjYmlBZ0lDQjlMRnh1ZlR0Y2JpSXNJbVY0Y0c5eWRDQmtaV1poZFd4MElIdGNiaUFnSUNCcGJtbDBLQ2w3WEc0Z0lDQWdJQ0FnSUdOdmJuTnZiR1V1Ykc5bktDZG5iRzlpWVd3Z2FXNXBkQ2NwTzF4dUlDQWdJSDFjYm4wN0lsMTkifQ==
